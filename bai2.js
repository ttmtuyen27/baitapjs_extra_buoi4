function findNumOfDay() {
  var month = document.getElementById("thang").value * 1;
  var year = document.getElementById("nam").value * 1;
  var isLeapYear = false;
  if (year % 4 == 0) {
    if (year % 100 == 0) {
      if (year % 400 == 0) isLeapYear = true;
    } else isLeapYear = true;
  }
  var maxday;
  switch (month) {
    case 1: {
      maxday = 31;
      break;
    }
    case 3: {
      maxday = 31;
      break;
    }
    case 4: {
      maxday = 30;
      break;
    }
    case 5: {
      maxday = 31;
      break;
    }
    case 6: {
      maxday = 30;
      break;
    }
    case 7: {
      maxday = 31;
      break;
    }
    case 8: {
      maxday = 31;
      break;
    }
    case 9: {
      maxday = 30;
      break;
    }
    case 10: {
      maxday = 31;
      break;
    }
    case 11: {
      maxday = 30;
      break;
    }
    case 12: {
      maxday = 31;
      break;
    }
  }
  if (month == 2 && isLeapYear == true) {
    maxday = 29;
  } else if (month == 2 && isLeapYear != true) {
    maxday = 28;
  }
  document.getElementById(
    "ketQuaBai2"
  ).innerHTML = `Tháng ${month}/${year} có ${maxday} ngày`;
  document.getElementById("ketQuaBai2").style.backgroundColor = "#d1ecf1";
  document.getElementById("ketQuaBai2").style.padding = "10px";
}
