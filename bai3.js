function cachDoc(n) {
  var result = null;
  switch (n) {
    case 1:
      result = "một";
      break;
    case 2:
      result = "hai";
      break;
    case 3:
      result = "ba";
      break;
    case 4:
      result = "bốn";
      break;
    case 5:
      result = "năm";
      break;
    case 6:
      result = "sáu";
      break;
    case 7:
      result = "bảy";
      break;
    case 8:
      result = "tám";
      break;
    case 9:
      result = "chín";
      break;
  }
  return result;
}
var docSo = function () {
  var num = document.getElementById("number").value * 1;
  var result;
  if (num % 100 == 0) {
    result = cachDoc(num / 100) + " trăm ";
  } else {
    result = cachDoc(Math.floor(num / 100)) + " trăm ";
    if (Math.floor(num / 10) % 10 == 0) {
      result += "lẻ ";
      result += cachDoc(num % 10);
    } else {
      if (Math.floor(num / 10) % 10 == 1) {
        result += "mười ";
        if (num % 10 == 5) result += "lăm";
        else if (num % 10 != 0) result += cachDoc(num % 10);
      } else {
        result += cachDoc(Math.floor(num / 10) % 10) + " mươi ";
        if (num % 10 == 1) result += "mốt";
        else if (num % 10 == 5) result += "lăm";
        else if (num % 10 != 0) result += cachDoc(num % 10);
      }
    }
  }
  document.getElementById("ketQuaBai3").innerHTML = result;
  document.getElementById("ketQuaBai3").style.backgroundColor = "#d1ecf1";
  document.getElementById("ketQuaBai3").style.padding = "10px";
};
