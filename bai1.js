function findNextDay() {
  var day = document.getElementById("day").value * 1;
  var month = document.getElementById("month").value * 1;
  var year = document.getElementById("year").value * 1;
  var isLeapYear = false;
  if (year % 4 == 0) {
    if (year % 100 == 0) {
      if (year % 400 == 0) isLeapYear = true;
    } else isLeapYear = true;
  }
  var maxday;
  switch (month) {
    case 1: {
      maxday = 31;
      break;
    }
    case 3: {
      maxday = 31;
      break;
    }
    case 4: {
      maxday = 30;
      break;
    }
    case 5: {
      maxday = 31;
      break;
    }
    case 6: {
      maxday = 30;
      break;
    }
    case 7: {
      maxday = 31;
      break;
    }
    case 8: {
      maxday = 31;
      break;
    }
    case 9: {
      maxday = 30;
      break;
    }
    case 10: {
      maxday = 31;
      break;
    }
    case 11: {
      maxday = 30;
      break;
    }
    case 12: {
      maxday = 31;
      break;
    }
  }
  if (month == 2 && isLeapYear == true) {
    maxday = 29;
  } else if (month == 2 && isLeapYear != true) {
    maxday = 28;
  }
  if (day != maxday) {
    day += 1;
  } else {
    day = 1;
    if (month == 12) {
      year += 1;
      month = 1;
    } else month += 1;
  }
  document.getElementById(
    "ketQuaB1"
  ).innerHTML = `Ngày mai: ${day}/${month}/${year}`;
  document.getElementById("ketQuaB1").style.backgroundColor = "#d1ecf1";
  document.getElementById("ketQuaB1").style.padding = "10px";
}

function findYesterday() {
  var day = document.getElementById("day").value * 1;
  var month = document.getElementById("month").value * 1;
  var year = document.getElementById("year").value * 1;
  var isLeapYear = false;
  if (year % 4 == 0) {
    if (year % 100 == 0) {
      if (year % 400 == 0) isLeapYear = true;
    } else isLeapYear = true;
  }
  var maxday;
  switch (month - 1) {
    case 1: {
      maxday = 31;
      break;
    }
    case 3: {
      maxday = 31;
      break;
    }
    case 4: {
      maxday = 30;
      break;
    }
    case 5: {
      maxday = 31;
      break;
    }
    case 6: {
      maxday = 30;
      break;
    }
    case 7: {
      maxday = 31;
      break;
    }
    case 8: {
      maxday = 31;
      break;
    }
    case 9: {
      maxday = 30;
      break;
    }
    case 10: {
      maxday = 31;
      break;
    }
    case 11: {
      maxday = 30;
      break;
    }
    case 0: {
      maxday = 31;
      break;
    }
  }
  if (month - 1 == 2 && isLeapYear == true) {
    maxday = 29;
  } else if (month - 1 == 2 && isLeapYear != true) {
    maxday = 28;
  }
  if (day != 1) {
    day -= 1;
  } else {
    day = maxday;
    if (month == 1) {
      year -= 1;
      month = 12;
    } else month -= 1;
  }
  document.getElementById(
    "ketQuaB1"
  ).innerHTML = `Hôm qua: ${day}/${month}/${year}`;
  document.getElementById("ketQuaB1").style.backgroundColor = "#d1ecf1";
  document.getElementById("ketQuaB1").style.padding = "10px";
}
