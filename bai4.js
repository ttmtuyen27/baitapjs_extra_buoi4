function tinhKhoangCach(x, y, x0, y0) {
  var distance = Math.sqrt((x - x0) * (x - x0) + (y - y0) * (y - y0));
  return distance;
}

function timNguoiXaNhat() {
  var x1 = document.getElementById("x1").value * 1;
  var x2 = document.getElementById("x2").value * 1;
  var x3 = document.getElementById("x3").value * 1;
  var y1 = document.getElementById("y1").value * 1;
  var y2 = document.getElementById("y2").value * 1;
  var y3 = document.getElementById("y3").value * 1;
  var x0 = document.getElementById("x0").value * 1;
  var y0 = document.getElementById("y0").value * 1;
  var name1 = document.getElementById("name1").value;
  var name2 = document.getElementById("name2").value;
  var name3 = document.getElementById("name3").value;
  distance1 = tinhKhoangCach(x1, y1, x0, y0);
  distance2 = tinhKhoangCach(x2, y2, x0, y0);
  distance3 = tinhKhoangCach(x3, y3, x0, y0);
  var result = null;
  if (distance1 >= distance2 && distance1 >= distance3) result = name1;
  else if (distance2 >= distance3 && distance2 >= distance1) result = name2;
  else result = name3;
  console.log({ result });
  document.getElementById(
    "ketQuaBai4"
  ).innerHTML = `Sinh viên xa trường nhất là: ${result}`;
  document.getElementById("ketQuaBai4").style.backgroundColor = "#d1ecf1";
  document.getElementById("ketQuaBai4").style.padding = "10px";
}
